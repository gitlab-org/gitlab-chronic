unless defined? Chronic
  $:.unshift File.expand_path('../../lib', __FILE__)
  require 'gitlab-chronic'
end

require 'minitest/autorun'

class TestCase < Minitest::Test
  def self.test(name, &block)
    define_method("test_#{name.gsub(/\W/, '_')}", &block) if block
  end
end
